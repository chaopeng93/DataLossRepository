# Benchmark for Data Loss errors in Android Apps

The benchmark features a collection of real data loss bugs found in open-source Android apps. The aim of the benchmark is to contribute to the study of data loss problems in Android apps by providing a solid collection of bugs on which to test techniques for data loss patching. Our work focused on the identification, compilation, execution and reproduction of data loss errors.

The benchmark includes open-source Android apps affected by data loss, their source code and apk files, a reference to the bug reports and commits about the errors, and a test case to reproduce each data loss bug automatically. You can find the instructions on how to use the benchmark below.

## Publications 

Riganelli O, Micucci D, Mariani L. A Benchmark of Data Loss Bugs for Android Apps. In Proceedings of the 16th Working Conference on Mining Software Repositories (MSR), Montreal, Canada, 2019.

## Related Datasets

[From Source Code to Test Cases: A Comprehensive Benchmark for Resource Leak Detection in Android Apps](https://github.com/riganelli/AndroidResourceLeaks)

## Requirements

1.  If not already present, install:
    
    *   [Andorid SDK](https://developer.android.com/)
    *   [Appium](http://appium.io/) and [IntelliJ IDEA](https://www.jetbrains.com/idea/download/) for test case execution
    *   [Android Studio](https://developer.android.com/studio/) if you want to re-compile and debug apps
    *   An Android device or emulator like [Genymotion](https://www.genymotion.com/fun-zone/) to run apps  
          
        
2.  Set "ANDROID\_HOME" to point to your Android SDK in the environment variables.


## Clone the repository

To clone the Data Loss repository on your Computer, press the Clone button in the Project tab, then select HTTPS or SSH from the dropdown menu and copy the link using the Copy URL to clipboard button.  
Go to your computer’s shell and type the following command with your SSH or HTTPS URL:

```
git clone *PASTE HTTPS OR SSH HERE*
```

## How to reproduce Data Loss errors

1)  Open the file *Data\_Loss\_Apps.htm* to see a tabular representation of the information and artifacts produced for each bug, as displayed below.
![*Data\_Loss\_Apps.htm* file](imgs/Data-Loss-Benchmark.png)

2)  The first column shows the ID assigned to each bug.

3)  The second column contains the name of the app analyzed with a reference to the software's project. For example, by clicking on AntennaPod you will open its official repository on GitHub.

![*Data\_Loss\_Apps.htm* file](imgs/AppName-AntennaPod.png)


4)  The third column features the app's category taken from the official Google Play Store page.

5)  The fourth column, Issue Report, contains a reference to the first report of the bug.

6)  The Faulty Version column contains a reference to the faulty version of the app in its repository. For example, by clicking on v1.5.2.0 for AntennaPod you will reach the following page:

![*Data\_Loss\_Apps.htm* file](imgs/FaultyVersion.png)


7)  The sixth column, the Faulty Source Code, features a link to the zip file containing the source code of the faulty version of the app.

8)  The Faulty APK column stores a link to the apk file of the faulty app.

9)  The Faulty Activity column shows the name of the activity exepriencing the data loss error.

10)  The ninth column, Fixed Version, refers to the version of the app in which the data loss has been fixed.

11)  The tenth column, Fixed APK, contains a link to the APK of the fixed version of the app.

12)  The Fixed Source Code column shows a link to the zip file containing a copy of the project sources with the fix.

13)  The twelfth column, Test Case, features a link to the zip file that contains an Appium automatic test case that can be executed to reveal the data loss error. 
To run the test case it is necessary to configure and run the Appium server. The server must run locally, its address must be 127.0.0.1 and its port 4723. 

![*Data\_Loss\_Apps.htm* file](imgs/AppiumServer-1.png)

Click on the Start Server button to launch the Appium Server. 

![*Data\_Loss\_Apps.htm* file](imgs/AppiumServer-2.png)


Install the APK you want to test on the device. You can use the code below to install application from command line:
```
adb install example.apk
```


Unzip the file containing the test case project and open it with IntelliJ IDEA. Now compile the project and run the test case on your device.

![*Data\_Loss\_Apps.htm* file](imgs/TestCase.png)


14)  The Oracle columns indicates if the test case includes an oracle.

15)  The Tested API column shows the API Level of the emulator we used to reproduce the bug. 

16)  Target API (Comp-Min) are respectively: the Android API Level declared as target in the manifest file, the Android API Level that we compiled, and the minimum Android API Level that an Android device must have to run the app.


## License

The projects referenced in the Benchmark are subject to their respective licenses.